package minerbeef.Skidoodle.check.combat;

import org.bukkit.event.EventHandler;

import minerbeef.Skidoodle.Skidoodle;
import minerbeef.Skidoodle.check.Check;
import minerbeef.Skidoodle.packets.events.PacketPlayerEvent;
import minerbeef.Skidoodle.packets.events.PacketPlayerType;
import minerbeef.Skidoodle.utils.Chance;

public class Twitch extends Check {
	public Twitch(Skidoodle Daedalus) {
		super("Twitch", "Twitch", Daedalus);

		this.setEnabled(true);
		this.setBannable(true);

		setMaxViolations(5);
	}

	@EventHandler
	public void Player(PacketPlayerEvent e) {
		if (e.getType() != PacketPlayerType.LOOK) {
			return;
		}
		if ((e.getPitch() > 90.1F) || (e.getPitch() < -90.1F)) {
			getDaedalus().logCheat(this, e.getPlayer(), null, Chance.HIGH, new String[0]);
		}
	}
}
