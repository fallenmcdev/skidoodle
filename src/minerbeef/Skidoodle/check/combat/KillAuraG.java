package minerbeef.Skidoodle.check.combat;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import minerbeef.Skidoodle.Skidoodle;
import minerbeef.Skidoodle.check.Check;
import minerbeef.Skidoodle.utils.Chance;

public class KillAuraG extends Check {
	
	private Map<UUID, Integer> verbose;
	
	public KillAuraG(Skidoodle Daedalus) {
		super("KillauraG", "Killaura (Type G)", Daedalus);
		
		setEnabled(true);
		setBannable(true);
		
		verbose = new HashMap<UUID, Integer>();
	}
	
	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) { 
		if(!(e.getDamager() instanceof Player)) {
			return;
		}
		
		Player player = (Player) e.getDamager();
		
		int verbose = this.verbose.getOrDefault(player.getUniqueId(), 0);
		
		if(player.isDead()) {
			verbose++;
		} else if(this.verbose.containsKey(player.getUniqueId())) {
			this.verbose.remove(player.getUniqueId());
			return;
		}
		
		if(verbose > 1) {
			verbose = 0;
			getDaedalus().logCheat(this, player, "Hit another player while dead.", Chance.HIGH, new String[0]);
		}
		
		this.verbose.put(player.getUniqueId(), verbose);
	}
	
	

}
