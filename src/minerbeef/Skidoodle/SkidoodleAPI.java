package minerbeef.Skidoodle;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import minerbeef.Skidoodle.check.Check;
import minerbeef.Skidoodle.check.other.Latency;
import minerbeef.Skidoodle.utils.C;
import minerbeef.Skidoodle.utils.Chance;

public class SkidoodleAPI {

	private static Skidoodle daedalus;

	@SuppressWarnings("unused")
	private Plugin plugin;

	public SkidoodleAPI(Plugin plugin) {
		this.plugin = plugin;
		daedalus = (Skidoodle) plugin;
	}

	public static List<Check> getChecks() {
		return daedalus.getChecks();
	}

	public static Integer getPing(Player player) {
		return Integer.valueOf(Math.round((Latency.getLag(player) / 2) * 6));
	}

	public static String getChanceString(Chance chance) {
		if (chance == Chance.HIGH) {
			return C.Red + "HIGH";
		}
		if (chance == Chance.LIKELY) {
			return C.Gold + "LIKELY";
		}
		return C.Gray + "UNKNOWN";
	}

}